package net.bobmandude9889.web;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import com.sun.net.httpserver.HttpServer;

public class WebServer extends JavaPlugin {

	HttpServer server;
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		
		try {
			server = HttpServer.create(new InetSocketAddress(getConfig().getString("ip"), getConfig().getInt("port")), 0);
			server.createContext("/players", new HttpPlayerHandler(this));
			
			ConfigurationSection sect = getConfig().getConfigurationSection("remap");
			for(String path : sect.getKeys(false)){
				server.createContext("/" + path, new HttpFileRemapHandler(this, sect.getString(path)));
			}
			
			server.createContext("/", new HttpFileHandler(this));
			server.setExecutor(null);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDisable() {
		server.stop(0);
	}
	
}
