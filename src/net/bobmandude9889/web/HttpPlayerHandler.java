package net.bobmandude9889.web;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class HttpPlayerHandler implements HttpHandler {

	WebServer plugin;

	public HttpPlayerHandler(WebServer plugin) {
		this.plugin = plugin;
	}

	@Override
	public void handle(HttpExchange e) throws IOException {
		JsonArray players = new JsonArray();
		
		for(Player player : Bukkit.getOnlinePlayers()){
			JsonObject playerObj = new JsonObject();
			
			playerObj.addProperty("name", player.getName());
			
			Location location = player.getLocation();
			JsonObject locationObj = new JsonObject();
			locationObj.addProperty("world", location.getWorld().getName());
			locationObj.addProperty("x", location.getBlockX());
			locationObj.addProperty("y", location.getBlockY());
			locationObj.addProperty("z", location.getBlockZ());
			
			playerObj.add("location", locationObj);
			
			players.add(playerObj);
		}
		
		HttpFileHandler.sendFile(players.toString().getBytes(), e, "text/plain");
	}

}
