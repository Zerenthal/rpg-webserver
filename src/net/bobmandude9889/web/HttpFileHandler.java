package net.bobmandude9889.web;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class HttpFileHandler implements HttpHandler {

	WebServer plugin;

	static File webFolder;
	
	static HashMap<String, String> fileTypes;
	
	public HttpFileHandler(WebServer plugin) {
		this.plugin = plugin;
		
		webFolder = new File(plugin.getDataFolder(),"website");
		webFolder.mkdir();
		
		fileTypes = new HashMap<String,String>();
		fileTypes.put("js", "text/javascript");
		fileTypes.put("css", "text/css");
		fileTypes.put("html", "text/html");
		fileTypes.put("htm", "text/html");
		fileTypes.put("png", "image/png");
		fileTypes.put("jpg", "text/jpeg");
		fileTypes.put("jpeg", "text/jpeg");
		fileTypes.put("gif", "text/gif");
		fileTypes.put("ttf", "application/x-font-ttf");
	}

	@Override
	public void handle(HttpExchange e) throws IOException {
		byte[] content = null;
		
		String path = e.getRequestURI().getPath();
		
		if (e.getRequestURI().getPath().equals("/")) {
			path = plugin.getConfig().getString("home");
		}
		
		String type = getType(path);
		
		content = loadFile(path);
		
		sendFile(content, e, type);
	}

	public static String getType(String path) {
		int index = path.lastIndexOf(".");
		
		String ext = null;
		
		if(index != -1)
			ext = path.substring(index + 1, path.length());
		
		String type = "text/html";
		
		if(ext != null && fileTypes.containsKey(ext.toLowerCase()))
			type = fileTypes.get(ext.toLowerCase());
		
		return type;
	}
	
	public static void sendFile(byte[] content, HttpExchange e, String type) {
		e.getResponseHeaders().set("Content-Type", type);
		
		try {
			e.sendResponseHeaders(200, content.length);
			
			e.getResponseBody().write(content);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		e.close();
	}
	
	public static byte[] loadFile(String path) {
		File file = new File(webFolder,path);
		
		if(!file.exists())
			file = new File(webFolder,"404.html");
		
		try {
			byte[] bytes = Files.readAllBytes(Paths.get(file.toURI()));
			return bytes;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
