package net.bobmandude9889.web;

import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class HttpFileRemapHandler implements HttpHandler {

	WebServer plugin;
	String filePath;
	
	public HttpFileRemapHandler(WebServer plugin, String file) {
		this.plugin = plugin;
		this.filePath = file;
	}
	
	@Override
	public void handle(HttpExchange e) throws IOException {
		byte[] content = HttpFileHandler.loadFile(filePath);
		HttpFileHandler.sendFile(content, e, HttpFileHandler.getType(filePath));
	}

}
